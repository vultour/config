colorscheme desert
set visualbell
set t_vb=

set showcmd
set showmatch
set noautoindent
set number
set laststatus=2

set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P

filetype plugin indent on

" Autocomplete config
set completeopt-=preview

" Fancy font for status bar
let g:airline_powerline_fonts = 1

" Syntastic config
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['cpp', 'c', 'asm'] }

" Golang config
let g:go_list_type = "quickfix"
let g:go_fmt_command = "goimports"
let g:go_addtags_transform = "camelCase"
let g:go_metalinter_autosave = 1
let g:go_list_autoclose = 1
let g:go_code_completion_enabled = 1

autocmd BufNewFile,BufRead *.go setlocal noexpandtab omnifunc=go#complete#Complete

" TypeScript Config
au BufNewFile,BufRead *.ts set filetype=javascript

" Tab config
set tabstop=4
set shiftwidth=4
set expandtab

" Red line @ 80
set colorcolumn=80

" Highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

" Automatically save the file on :make or :GoBuild
set autowrite

" netrw config
let g:netrw_liststyle = 1
